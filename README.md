# aws-s3.cr

AWS S3 SDK for Crystal.

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  aws-s3:
    gitlab: a-watson/aws-s3.cr
```

## Usage

```crystal
require "aws-s3"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/a-watson/aws-s3.cr/fork/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [a-watson](https://gitlab.com/a-watson) Adam Watson - creator, maintainer
